<?php

namespace App\Controllers;
use App\Models\Page;

class PagesController
{
	public function home()
	{
		$pages = Page::selectAll();
		return view('index', compact('pages'));

		return view('index');
	}

	public function pageFilter($data)
	{
		header('Content-type: application/json');
		echo json_encode([
			'code' => 200,
			'data' => $data
		]);
	}

	public function errorPage($message)
	{
		header('Content-type: application/json');
		echo json_encode([
			'code' => 404,
			'message' => $message
		]);
	}
} 