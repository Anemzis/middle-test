<?php

namespace App\Models;
use App\Core\App;

class Page
{
	public static function selectAll()
	{
		return App::get('database')->selectAll('pages');
	}

	public static function selectPageBySlug($slug)
	{
		return App::get('database')->select('pages','*',"friendly = '$slug'");
	}

}