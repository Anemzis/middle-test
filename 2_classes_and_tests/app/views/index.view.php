<?php require('partials/head.php') ?>

<h1>Home page</h1>

<?php foreach ($pages as $page) : ?>
    <li>
        <a href="/<?php echo $page->friendly; ?>"><?php echo $page->title; ?></a>
    </li>
<?php endforeach; ?>

<?php require('partials/footer.php') ?>