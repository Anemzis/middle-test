<?php

class DataTest extends \PHPUnit_FrameWork_TestCase
{
	protected $client;

	protected function setUp()
	{
		$this->client = new GuzzleHttp\Client([
			'base_uri' => 'http://localhost:8000'
		]);
	}

	public function testCorretUrlData()
	{
		$response = $this->client->get('/page1');
		$this->assertEquals(200, $response->getStatusCode());
		$data = json_decode($response->getBody(), true);
		$this->assertArrayHasKey('code', $data);
		$this->assertArrayHasKey('data', $data);
		$this->assertEquals($data['data']['friendly'], 'page1');
		$this->assertEquals($data['data']['title'], 'Title 1');
		$this->assertEquals($data['data']['description'], 'Description 1');
	}

	public function testWrongUrlData()
	{
		$response = $this->client->get('/wrongpage');
		$this->assertEquals(200, $response->getStatusCode());
		$data = json_decode($response->getBody(), true);
		$this->assertArrayHasKey('code', $data);
		$this->assertArrayHasKey('message', $data);
		$this->assertEquals($data['message'], 'No route defined for this URI');
	}
}