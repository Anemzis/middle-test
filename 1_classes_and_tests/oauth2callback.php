<?php
require_once __DIR__.'/vendor/autoload.php';

define( 'APPLICATION_NAME', 'Drive Test' );
define( 'CREDENTIALS_PATH', __DIR__ . '/credentials/google-drive-credentials-http.json' );
define( 'CLIENT_SECRET_PATH', __DIR__ . '/client_secrets.json' );
define( 'SCOPES', Google_Service_Drive::DRIVE_METADATA_READONLY);


use App\GoogleDrive\CredentialsRepositoryFile;
use App\GoogleDrive\GoogleClientFactory;

$credentialsRepository = new CredentialsRepositoryFile( CREDENTIALS_PATH );

$factory = new GoogleClientFactory( APPLICATION_NAME,
	SCOPES,
	CLIENT_SECRET_PATH,
	$credentialsRepository);
if ( isset( $_GET['code'] ) ) {
	$factory->create( $_GET['code'] );
	$redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/';
	header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
} else {
	die( 'something went wrong....' );
}