<?php

namespace App\GoogleDrive;

interface CredentialsRepository
{
	public function loadAccessToken();
	public function storeAccessToken( $token );
}