<?php

namespace App\GoogleDrive;

use Google_Client;
use Google_Service_Drive;
use Carbon\Carbon;

class GoogleDriveWrapper
{
	private $client;
	private $service;

	public function __construct(Google_Client $client)
	{
		$this->client = $client;
		$this->service = new Google_Service_Drive($this->client);
	}


	protected function list($optParams = [])
	{
		//$optParams = $optParams ?? [];
		$request = $this->service->files->listFiles($optParams);
		$foo = new \Google_Http_Batch( $this->client );
		$foo->add( $request );
		$response = $foo->execute();
		$results = array_pop( $response );
		if ( $results instanceof \Google_Service_Exception ) {
			var_dump( $results );
			throw new \Exception( 'Google Service returned Exception' );
		} else {
			return $results->getFiles();
		}
	}

	public function listLastMonth()
	{
		$params = array(
			//'q' => "modifiedTime > '".date('c', strtotime('-1 month'))."'",
			'q' => "modifiedTime > '".Carbon::now()->subMonths(1)->format('c')."'",
			'fields' => 'files(id, name, modifiedTime)'
		);
		$files = $this->list($params);

		if (count($files) == 0) {
			print "No files found.\n";
		} else {
			foreach ($files as $file) {
				printf("File name: %s, Last modifed: %s\n<br>", $file->name, Carbon::parse($file->modifiedTime)->format('l jS \\of F Y h:i:s A'));
			}
		}
	}
}