<?php

namespace App\GoogleDrive;

class CredentialsRepositoryFile implements CredentialsRepository
{

	private $credentialsPath;
	public function __construct( string $credentialsPath )
	{
		$this->credentialsPath = $this->expandHomeDirectory( $credentialsPath );
	}

	public function loadAccessToken() :string
	{
		if (file_exists( $this->credentialsPath )) {
			return file_get_contents( $this->credentialsPath );
		}
		return '';
	}
	public function storeAccessToken( $accessToken )
	{
		if ( !file_exists( dirname( $this->credentialsPath ) )) {
			mkdir( dirname( $this->credentialsPath ), 0700, true );
		}
		file_put_contents( $this->credentialsPath, $accessToken );
	}

	private function expandHomeDirectory( $path )
	{
		$homeDirectory = getenv( 'HOME' );
		if ( empty($homeDirectory) ) {
			$homeDirectory = getenv("HOMEDRIVE") . getenv("HOMEPATH");
		}
		return str_replace('~', realpath( $homeDirectory ), $path);
	}
}